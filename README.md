# KADO

Kitware Annotation and Dataset Ontology

KADO is a lightweight knowledge graph notation for gluing together data and metadata.

It’s JSON-LD, but stripped down, geared towards local filesystems, and less strict about context

...but wait, what is [JSON-LD](https://json-ld.org/learn.html)?

It's JSON with reserved keywords, types, and uses URI links to structure data.


todo: 
* [ ] figure out how to get relative contexts for prototyping

References: 

[XML Schema Build-in Datatypes](https://www.w3.org/TR/rdf11-concepts/#xsd-datatypes)
[XML Schema XSD Part 2: Datatypes](https://www.w3.org/TR/xmlschema11-2/)