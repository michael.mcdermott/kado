# MEVA Vocabulary

This is an example vocab for use with KPF->KADO demo. 
These are not the full terms and merely a demonstration. 

# detectionID
AKA "id0"

# trackID
AKA "id1"

# eventID
AKA "id2"

# annotation
An object of type `annotation`

# event
An `annotation` object representing some event

## src_status
Specifies the source of an event

## actors
Agents which perform an activity in an event

# annotations
A list of annotations

## timespan
A length of time

### frame_num
A tuple of frame numbers `[start, end]` in standard slice notation (start <= i < end)

## label
A categorical label combined with a likelihood assigned by a detector

### likelihood
A floating-point probability measure from 0 to 1. 

### label_name
A human-readable name describing the label assigned 