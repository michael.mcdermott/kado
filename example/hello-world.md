# KADO Hello World

This file is an example ontology demonstrating a minimalist implementation of
the KADO concept. This is the whole thing. No LD, no RDF, just secctions 
accessible by `#fragment` identifiers and human-readable descriptions of the
semantics.

# author

The person who wrote it. 

# images

A list of links to images

## alt

An alternate description of an image

# references

A list of links to external sources