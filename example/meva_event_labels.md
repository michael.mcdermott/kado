# MEVA Event Vocabulary

This is an example vocab for use with KPF->KADO demo. 
These are not the full terms and merely a demonstration. 

# person_talks_to_person
A person talks to another person

# person_stands_up
A person goes from sitting to standing

# person_picks_up_object
A person picks up an object with their hand