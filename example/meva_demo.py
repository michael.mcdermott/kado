import black
import json
from pyld import jsonld


def dict2black(d, line_length=120):
    """Black-ifies a dict. More compact than pprint"""
    return black.format_str(json.dumps(d), mode=black.FileMode(line_length=line_length))


def disambiguate(lddict):
    context = {}
    exp = jsonld.expand(lddict)
    return jsonld.compact(exp, context)


def recompact(lddict):
    context = lddict["@context"]
    exp = jsonld.expand(lddict)
    comp2 = jsonld.compact(exp, context)
    return comp2


with open("meva.jsonld") as fp:
    data_in = json.load(fp)

print("===" * 3 + " Input: " + "===" * 3)
print(dict2black(data_in))

print("===" * 3 + " Disambiguated: " + "===" * 3)
data_disambiguated = disambiguate(data_in)
print(dict2black(data_disambiguated))

print("===" * 3 + " Recompacted is the same as input: " + "===" * 3)
data_recompacted = recompact(data_in)
matches = data_in == data_recompacted
print(matches)
